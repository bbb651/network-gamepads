use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/gitlab/bbb651/NetworkGamepads/window.ui")]
    pub struct NetworkGamepadsWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NetworkGamepadsWindow {
        const NAME: &'static str = "NetworkGamepadsWindow";
        type Type = super::NetworkGamepadsWindow;
        type ParentType = gtk::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for NetworkGamepadsWindow {}
    impl WidgetImpl for NetworkGamepadsWindow {}
    impl WindowImpl for NetworkGamepadsWindow {}
    impl ApplicationWindowImpl for NetworkGamepadsWindow {}
}

glib::wrapper! {
    pub struct NetworkGamepadsWindow(ObjectSubclass<imp::NetworkGamepadsWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl NetworkGamepadsWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create NetworkGamepadsWindow")
    }
}
